package com.example.tripschedule.ui.Schedule;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EditScheduleFragmentTest {

    private EditScheduleFragment editScheduleFragment;

    @Before
    public void createEditScheduleFragment() {
        editScheduleFragment = new EditScheduleFragment();
    }

    @Test
    public void getTime() {
        String actualTime1 = editScheduleFragment.getTime(12 , 20);
        String actualTime2 = editScheduleFragment.getTime(9 , 20);
        String actualTime3 = editScheduleFragment.getTime(13 , 2);
        String actualTime4 = editScheduleFragment.getTime(3 , 2);

        assertEquals("12:20", actualTime1);
        assertEquals("09:20", actualTime2);
        assertEquals("13:02", actualTime3);
        assertEquals("03:02", actualTime4);

    }

}