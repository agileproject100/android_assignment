package com.example.tripschedule.ui.Schedule;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AddScheduleFragmentTest {

    private AddScheduleFragment addScheduleFragment;

    @Before
    public void createAddScheduleFragmentTest() {
        addScheduleFragment = new AddScheduleFragment();
    }

    @Test
    public void getTime() {
        String actualTime1 = addScheduleFragment.getTime(12 , 20);
        String actualTime2 = addScheduleFragment.getTime(9 , 20);
        String actualTime3 = addScheduleFragment.getTime(13 , 2);
        String actualTime4 = addScheduleFragment.getTime(3 , 2);

        assertEquals("12:20", actualTime1);
        assertEquals("09:20", actualTime2);
        assertEquals("13:02", actualTime3);
        assertEquals("03:02", actualTime4);

    }

}