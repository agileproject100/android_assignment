package com.example.tripschedule.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserModelTest {

    private UserModel userModel;

    @Before
    public void createUserModel() {
        userModel = new UserModel();
    }

    @Test
    public void testGetSet() {

        UserModel UserModelTest =  new UserModel();
        UserModelTest.setUserId("3426");
        userModel.setUserId("3426");
        assertEquals(UserModelTest.getUserId(), userModel.getUserId());
        UserModelTest.setUserName("Tim Chan");
        userModel.setUserName("Tim Chan");
        assertEquals(UserModelTest.getUserName(), userModel.getUserName());
        UserModelTest.setEmail("Chan@gmail.com");
        userModel.setEmail("Chan@gmail.com");
        assertEquals(UserModelTest.getEmail(), userModel.getEmail());
        UserModelTest.setPassword("Chan1234");
        userModel.setPassword("Chan1234");
        assertEquals(UserModelTest.getPassword(), userModel.getPassword());

        UserModel UserModelTestCs =  new UserModel("3426" , "Tim Chan" , "Chan@gmail.com" , "Chan1234" );
        assertEquals(UserModelTestCs.getUserId(), userModel.getUserId());
        assertEquals(UserModelTestCs.getUserName(), userModel.getUserName());
        assertEquals(UserModelTestCs.getEmail(), userModel.getEmail());
        assertEquals(UserModelTestCs.getPassword(), userModel.getPassword());
    }

}