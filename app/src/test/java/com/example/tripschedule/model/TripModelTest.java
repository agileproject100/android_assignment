package com.example.tripschedule.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TripModelTest {

    private TripModel tripModel;

    @Before
    public void createTripModel() {
        tripModel = new TripModel();
    }


    @Test
    public void testGetSet(){


        TripModel tripModelTest =  new TripModel();
        tripModelTest.setUid("5436756");
        tripModel.setUid("5436756");
        assertEquals(tripModelTest.getUid(), tripModel.getUid());

        tripModelTest.setTripName("First Trip");
        tripModel.setTripName("First Trip");
        assertEquals(tripModelTest.getTripName(), tripModel.getTripName());

        tripModelTest.setDescription("This is my First Trip");
        tripModel.setDescription("This is my First Trip");
        assertEquals(tripModelTest.getDescription(), tripModel.getDescription());

        TripModel tripModelTestCs =  new TripModel("5436756" , "First Trip" , "This is my First Trip");

        assertEquals(tripModelTestCs.getUid(), tripModel.getUid());
        assertEquals(tripModelTestCs.getTripName(), tripModel.getTripName());
        assertEquals(tripModelTestCs.getDescription(), tripModel.getDescription());

    }

}