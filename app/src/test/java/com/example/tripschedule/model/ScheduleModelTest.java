package com.example.tripschedule.model;

import com.example.tripschedule.ui.Schedule.EditScheduleFragment;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ScheduleModelTest {

    private ScheduleModel scheduleModel;

    @Before
    public void createScheduleModel() {
        scheduleModel = new ScheduleModel();
    }

    @Test
    public void testGetSet() {


        ScheduleModel scheduleModelTest =  new ScheduleModel();
        scheduleModelTest.setFromTime("11:30");
        scheduleModel.setFromTime("11:30");
        assertEquals(scheduleModelTest.getFromTime(), scheduleModel.getFromTime());
        scheduleModelTest.setToTime("15:30");
        scheduleModel.setToTime("15:30");
        assertEquals(scheduleModelTest.getToTime(), scheduleModel.getToTime());
        scheduleModelTest.setFromDate("2021/05/31");
        scheduleModel.setFromDate("2021/05/31");
        assertEquals(scheduleModelTest.getFromDate(), scheduleModel.getFromDate());
        scheduleModelTest.setToDate("2021/06/02");
        scheduleModel.setToDate("2021/06/02");
        assertEquals(scheduleModelTest.getToDate(), scheduleModel.getToDate());
        scheduleModelTest.setFromDestination("IVE(ST)");
        scheduleModel.setFromDestination("IVE(ST)");
        assertEquals(scheduleModelTest.getFromDestination(), scheduleModel.getFromDestination());
        scheduleModelTest.setToDestination("IVE(TY)");
        scheduleModel.setToDestination("IVE(TY)");
        assertEquals(scheduleModelTest.getToDestination(), scheduleModel.getToDestination());
        scheduleModelTest.setTripId("435");
        scheduleModel.setTripId("435");
        assertEquals(scheduleModelTest.getTripId(), scheduleModel.getTripId());

        ScheduleModel scheduleModelTestcs =  new ScheduleModel("435" , "IVE(ST)" , "IVE(TY)" , "2021/05/31" , "2021/06/02" , "11:30" , "15:30" );

        assertEquals(scheduleModelTestcs.getFromTime(), scheduleModel.getFromTime());
        assertEquals(scheduleModelTestcs.getToTime(), scheduleModel.getToTime());
        assertEquals(scheduleModelTestcs.getFromDate(), scheduleModel.getFromDate());
        assertEquals(scheduleModelTestcs.getToDate(), scheduleModel.getToDate());
        assertEquals(scheduleModelTestcs.getFromDestination(), scheduleModel.getFromDestination());
        assertEquals(scheduleModelTestcs.getToDestination(), scheduleModel.getToDestination());
        assertEquals(scheduleModelTestcs.getTripId(), scheduleModel.getTripId());


    }

}