package com.example.tripschedule.ui.Trip;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tripschedule.R;
import com.example.tripschedule.model.TripModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TripPlanningFragment extends Fragment {
    public static View v;
    private RecyclerView trip_recycleView;
    private FirebaseAuth mAuth;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_trip_planning, container, false);

        setHasOptionsMenu(true);

        trip_recycleView = v.findViewById(R.id.trip_recycleView);
        mAuth = FirebaseAuth.getInstance();


        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Trips");

        ref.orderByChild("uid").equalTo(mAuth.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<TripModel> tripModels = new ArrayList<>();
                List<String> tripIds = new ArrayList<>();
                for (DataSnapshot SnapshotItem : dataSnapshot.getChildren()) {
                    tripIds.add(SnapshotItem.getKey());
                    tripModels.add(new TripModel(
                            SnapshotItem.child("uid").getValue().toString(),
                            SnapshotItem.child("tripName").getValue().toString(),
                            SnapshotItem.child("description").getValue().toString()
                    ));
                }
                TripItemAdapter tripItemAdapter = new TripItemAdapter(getContext(),tripModels,tripIds);
                trip_recycleView.setAdapter(tripItemAdapter);

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        return v;
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_nav, menu);

        MenuItem Add = menu.findItem(R.id.addtrip);

        Add.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Navigation.findNavController(v).navigate(R.id.action_tripPlanningFragment_to_addTripFragment);

                return false;
            }
        });

    }
}