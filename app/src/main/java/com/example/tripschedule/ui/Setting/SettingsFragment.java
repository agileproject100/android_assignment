package com.example.tripschedule.ui.Setting;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.example.tripschedule.MainActivity;
import com.example.tripschedule.R;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Locale;

public class SettingsFragment extends PreferenceFragmentCompat {

    private FirebaseAuth mAuth;
    @NonNull
    private Preference logout;
    private ListPreference languageList;
    private Locale myLocale;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey);

        mAuth = FirebaseAuth.getInstance();

        logout = findPreference("logout");
        languageList = findPreference("language");

        logout.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent logoutIntent = new Intent(getActivity(), MainActivity.class);
                mAuth.signOut();
                startActivity(logoutIntent);
                getActivity().finish();
                return true;
            }
        });

        languageList.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Toast.makeText(getContext(),"Please restart the app to affect your setting.", Toast.LENGTH_LONG).show();
                return true;
            }
        });


    }
}