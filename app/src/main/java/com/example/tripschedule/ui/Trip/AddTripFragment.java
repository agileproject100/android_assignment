package com.example.tripschedule.ui.Trip;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.tripschedule.R;
import com.example.tripschedule.model.TripModel;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class AddTripFragment extends Fragment {
    private TextInputEditText title_edittext;
    private TextInputEditText description_edittext;
    private TextInputLayout triptitle_outlinedTextField;
    private Button add_bt;
    private DatabaseReference ref;
    private FirebaseAuth mAuth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_add_trip, container, false);
        title_edittext = v.findViewById(R.id.title_edittext);
        description_edittext = v.findViewById(R.id.description_edittext);
        triptitle_outlinedTextField = v.findViewById(R.id.triptitle_outlinedTextField);
        add_bt = v.findViewById(R.id.add_bt);
        mAuth = FirebaseAuth.getInstance();




        title_edittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if(title_edittext.getText().length() == 0){
                    // Set error text
                    triptitle_outlinedTextField.setError(getString(R.string.error_text));
                }else{
                    // Clear error text
                    triptitle_outlinedTextField.setError(null);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(title_edittext.getText().length() == 0){
                    // Set error text
                    triptitle_outlinedTextField.setError(getString(R.string.error_text));
                }else{
                    // Clear error text
                    triptitle_outlinedTextField.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(title_edittext.getText().length() == 0){
                    // Set error text
                    triptitle_outlinedTextField.setError(getString(R.string.error_text));
                }else{
                    // Clear error text
                    triptitle_outlinedTextField.setError(null);
                }
            }
        });


        add_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(title_edittext.getText().length() == 0){
                    // Set error text
                    triptitle_outlinedTextField.setError(getString(R.string.error_text));
                }else{

                    ref = FirebaseDatabase.getInstance().getReference().child("Trips");
                    TripModel tripModel = new TripModel( mAuth.getUid() ,title_edittext.getText().toString().trim() , description_edittext.getText().toString().trim() );
                    ref.push().setValue(tripModel);
                    Navigation.findNavController(v).navigate(R.id.action_addTripFragment_self);

                }
            }
        });


        return v;
    }
}