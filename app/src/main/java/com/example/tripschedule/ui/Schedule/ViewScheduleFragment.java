package com.example.tripschedule.ui.Schedule;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.tripschedule.R;
import com.example.tripschedule.model.ScheduleModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class ViewScheduleFragment extends Fragment {

    private String scheduleId;
    private DatabaseReference ref;
    private TextView from_destination_textView , to_destination_textView , from_date_textView , to_date_textView , from_time_textView , to_time_textView;
    private Button edit_bt , delete_bt;
    public ScheduleModel scheduleModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_view_schedule, container, false);
        scheduleId = getArguments().getString("scheduleId");
        ref = FirebaseDatabase.getInstance().getReference().child("Schedules");

        from_destination_textView = v.findViewById(R.id.from_destination_textView);
        to_destination_textView = v.findViewById(R.id.to_destination_textView);
        from_date_textView = v.findViewById(R.id.from_date_textView);
        to_date_textView = v.findViewById(R.id.to_date_textView);
        from_time_textView = v.findViewById(R.id.from_time_textView);
        to_time_textView = v.findViewById(R.id.to_time_textView);
        edit_bt = v.findViewById(R.id.edit_bt);
        delete_bt = v.findViewById(R.id.delete_bt);



        ref.orderByKey().equalTo(scheduleId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot SnapshotItem : snapshot.getChildren()) {
                    scheduleModel = new ScheduleModel(
                            SnapshotItem.child("tripId").getValue().toString(),
                            SnapshotItem.child("fromDestination").getValue().toString(),
                            SnapshotItem.child("toDestination").getValue().toString(),
                            SnapshotItem.child("fromDate").getValue().toString(),
                            SnapshotItem.child("toDate").getValue().toString(),
                            SnapshotItem.child("fromTime").getValue().toString(),
                            SnapshotItem.child("toTime").getValue().toString()
                    );
                }
                from_destination_textView.setText( getResources().getString(R.string.from_destination_view_schedule_screen) +  scheduleModel.getFromDestination());
                to_destination_textView.setText( getResources().getString(R.string.to_destination_view_schedule_screen) +  scheduleModel.getToDestination());
                from_date_textView.setText( getResources().getString(R.string.from_date_view_schedule_screen) +  scheduleModel.getFromDate());
                to_date_textView.setText( getResources().getString(R.string.to_date_view_schedule_screen) +  scheduleModel.getToDate());
                from_time_textView.setText( getResources().getString(R.string.from_time_view_schedule_screen) +  scheduleModel.getFromTime());
                to_time_textView.setText( getResources().getString(R.string.to_time_view_schedule_screen) +  scheduleModel.getToTime());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        edit_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();
                bundle.putSerializable("scheduleModel", scheduleModel);
                bundle.putString("scheduleId", scheduleId);
                Navigation.findNavController(v).navigate(R.id.action_viewScheduleFragment_to_editScheduleFragment , bundle);

            }
        });

        delete_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ref.child(scheduleId).removeValue();
                Navigation.findNavController(v).navigate(R.id.action_viewScheduleFragment_self);

            }
        });

        return v;
    }
}