package com.example.tripschedule.ui.Schedule;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;


import com.example.tripschedule.R;
import com.example.tripschedule.model.ScheduleModel;
import com.example.tripschedule.ui.Trip.TripPlanningFragment;

import java.util.List;


public class ScheduleItemAdapter  extends RecyclerView.Adapter<ScheduleItemAdapter.Viewholder>{

    private List<ScheduleModel> scheduleModels;
    private List<String> scheduleIds;
    private Context context;

    ScheduleItemAdapter(Context context , List<ScheduleModel> scheduleModels , List<String> scheduleIds){
        this.context = context;
        this.scheduleModels = scheduleModels;
        this.scheduleIds = scheduleIds;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ScheduleItemAdapter.Viewholder(
                LayoutInflater.from(context)
                        .inflate(
                                R.layout.schedule_info_intem,
                                parent,
                                false
                        )
        );
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        ScheduleModel scheduleModel = scheduleModels.get(position);
        String scheduleId = scheduleIds.get(position);
        holder.destination_textView.setText(this.context. getResources().getString(R.string.start_infor_screen) + " : " +  scheduleModel.getFromDestination() +
                "\n" + this.context. getResources().getString(R.string.end_infor_screen) + " : "  + scheduleModel.getToDestination() );

        holder.schedule_item_linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("scheduleId", scheduleId);
                Navigation.findNavController(v).navigate(R.id.action_scheduleFragment_to_viewScheduleFragment , bundle);
            }
        });

    }

    @Override
    public int getItemCount() {
        return scheduleModels.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        private TextView destination_textView;
        private LinearLayout schedule_item_linearLayout;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            destination_textView = itemView.findViewById(R.id.destination_textView);
            schedule_item_linearLayout = itemView.findViewById(R.id.schedule_item_linearLayout);

        }

    }

}
