package com.example.tripschedule.ui.Schedule;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tripschedule.R;
import com.example.tripschedule.model.ScheduleModel;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


public class ScheduleFragment extends Fragment {

    private View v;
    private Button delete_bt;
    private TextView trip_name_textView;
    private RecyclerView schedule_recycleView;
    private String tripId;
    private String tripName;
    private DatabaseReference ref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_schedule, container, false);

        setHasOptionsMenu(true);
        schedule_recycleView = v.findViewById(R.id.schedule_recycleView);
        trip_name_textView = v.findViewById(R.id.trip_name_textView);
        delete_bt = v.findViewById(R.id.delete_bt);

        tripId = getArguments().getString("tripId");
        tripName = getArguments().getString("tripName");

        trip_name_textView.setText(tripName);

        ref = FirebaseDatabase.getInstance().getReference().child("Schedules");

        ref.orderByChild("tripId").equalTo(tripId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                List<ScheduleModel> scheduleModelList = new ArrayList<>();
                List<String> scheduleIds = new  ArrayList<>();
                for (DataSnapshot SnapshotItem : snapshot.getChildren()) {
                    scheduleIds.add(SnapshotItem.getKey());
                    scheduleModelList.add(new ScheduleModel(
                            SnapshotItem.child("tripId").getValue().toString(),
                            SnapshotItem.child("fromDestination").getValue().toString(),
                            SnapshotItem.child("toDestination").getValue().toString(),
                            SnapshotItem.child("fromDate").getValue().toString(),
                            SnapshotItem.child("toDate").getValue().toString(),
                            SnapshotItem.child("fromTime").getValue().toString(),
                            SnapshotItem.child("toTime").getValue().toString()
                    ));
                }
                ScheduleItemAdapter scheduleItemAdapter = new ScheduleItemAdapter(getContext(),scheduleModelList , scheduleIds);
                schedule_recycleView.setAdapter(scheduleItemAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        delete_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ref = FirebaseDatabase.getInstance().getReference().child("Trips");
                ref.child(tripId).removeValue();
                ref = FirebaseDatabase.getInstance().getReference().child("Schedules");
                ref.orderByChild("tripId").equalTo(tripId).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        for (DataSnapshot Snapshot: snapshot.getChildren()) {
                            Snapshot.getRef().removeValue();
                        }

                        Navigation.findNavController(v).navigate(R.id.action_scheduleFragment_self );
                        Toast.makeText(getContext(),getResources().getString(R.string.schedules_delete_success),Toast.LENGTH_LONG);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

            }
        });

        return v;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.schedule_bar, menu);

        MenuItem Add = menu.findItem(R.id.add_schedule);

        Add.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                Bundle bundle = new Bundle();
                bundle.putString("tripId", tripId);
                Navigation.findNavController(v).navigate(R.id.action_scheduleFragment_to_addScheduleFragment , bundle);

                return false;
            }
        });

    }

}