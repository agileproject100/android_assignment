package com.example.tripschedule.ui.Trip;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tripschedule.R;
import com.example.tripschedule.model.TripModel;

import java.util.List;

public class TripItemAdapter extends RecyclerView.Adapter<TripItemAdapter.Viewholder> {
    private List<TripModel> tripModels;
    private Context context;
    private List<String> tripIds;

    TripItemAdapter(Context context, List<TripModel> tripModels , List<String> tripIds) {
        this.tripModels = tripModels;
        this.context = context;
        this.tripIds = tripIds;
    }


    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new TripItemAdapter.Viewholder(
                LayoutInflater.from(context)
                        .inflate(
                                R.layout.trip_info_item,
                                parent,
                                false
                        )
        );
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {
        TripModel TM =tripModels.get(position);
        String tripId = tripIds.get(position);
        holder.triptitle_textView.setText(tripModels.get(position).getTripName());
        holder.description_textView.setText(tripModels.get(position).getDescription());

        holder.trip_item_linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("tripId", tripId);
                bundle.putString("tripName", tripModels.get(position).getTripName());

//                Toast.makeText(context,TM.getTripName(),Toast.LENGTH_LONG).show();
                Navigation.findNavController(TripPlanningFragment.v).navigate(R.id.action_tripPlanningFragment_to_scheduleFragment, bundle);

            }
        });

    }

    @Override
    public int getItemCount() {
        return tripModels.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        private TextView triptitle_textView, description_textView;
        private LinearLayout trip_item_linearLayout;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            triptitle_textView = itemView.findViewById(R.id.triptitle_textView);
            description_textView = itemView.findViewById(R.id.description_textView);
            trip_item_linearLayout = itemView.findViewById(R.id.trip_item_linearLayout);

        }
    }
}
