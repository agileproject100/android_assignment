package com.example.tripschedule.ui.Schedule;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tripschedule.BuildConfig;
import com.example.tripschedule.R;
import com.example.tripschedule.model.ScheduleModel;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.PlaceLikelihood;
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest;
import com.google.android.libraries.places.api.net.FindCurrentPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.timepicker.MaterialTimePicker;
import com.google.android.material.timepicker.TimeFormat;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class AddScheduleFragment extends Fragment {

    private TextInputLayout from_destination_outlinedTextField;
    private TextInputEditText from_destination_edittext;
    private TextInputLayout to_destination_outlinedTextField;
    private TextInputEditText to_destination_edittext;

    private TextView from_date_textView, to_date_textView, from_time_textView, to_time_textView;
    private Button from_date_button, to_date_button, from_time_button, to_time_button, get_location_bt, add_bt ;

    private ScheduleModel scheduleModel;
    private DatabaseReference ref;

    private String tripId;
    public String from_date, to_date, from_time, to_time;

    private static int AUTOCOMPLETE_REQUEST_CODE_F = 1;
    private static int AUTOCOMPLETE_REQUEST_CODE_T = 2;

    private static int ACCESS_FINE_LOCATION_PM = 100;

    public PlacesClient placesClient;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_add_schedule, container, false);

        from_destination_outlinedTextField = v.findViewById(R.id.from_destination_outlinedTextField);
        from_destination_edittext = v.findViewById(R.id.from_destination_edittext);
        to_destination_outlinedTextField = v.findViewById(R.id.to_destination_outlinedTextField);
        to_destination_edittext = v.findViewById(R.id.to_destination_edittext);

        from_date_textView = v.findViewById(R.id.from_date_textView);
        to_date_textView = v.findViewById(R.id.to_date_textView);

        from_time_textView = v.findViewById(R.id.from_time_textView);
        to_time_textView = v.findViewById(R.id.to_time_textView);

        from_date_button = v.findViewById(R.id.from_date_button);
        to_date_button = v.findViewById(R.id.to_date_button);

        from_time_button = v.findViewById(R.id.from_time_button);
        to_time_button = v.findViewById(R.id.to_time_button);

        get_location_bt = v.findViewById(R.id.get_location_bt);
        add_bt = v.findViewById(R.id.add_bt);


        scheduleModel = new ScheduleModel();
        tripId = getArguments().getString("tripId");

        Places.initialize(getContext(), BuildConfig.MAPS_API_KEY);
        placesClient = Places.createClient(getContext());


        from_date_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDate(getResources().getString(R.string.from_date_add_schedule_screen), from_date_textView);
            }
        });

        to_date_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDate(getResources().getString(R.string.to_date_add_schedule_screen), to_date_textView);
            }
        });

        from_time_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickTime(getResources().getString(R.string.from_time_add_schedule_screen), from_time_textView);
            }
        });

        to_time_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickTime(getResources().getString(R.string.to_time_add_schedule_screen), to_time_textView);
            }
        });




        from_destination_edittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME);
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields).build(getContext());
                    startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE_F);
                }
            }
        });

        to_destination_edittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME);
                    Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields).build(getContext());
                    startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE_T);
                }
            }
        });


        get_location_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get_location_bt.setEnabled(false);
                // Use fields to define the data types to return.
                List<Place.Field> placeFields = Collections.singletonList(Place.Field.NAME);

                // Use the builder to create a FindCurrentPlaceRequest.
                FindCurrentPlaceRequest request = FindCurrentPlaceRequest.newInstance(placeFields);

                // Call findCurrentPlace and handle the response (first check that the user has granted permission).
                if (ContextCompat.checkSelfPermission(getContext(), ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    Task<FindCurrentPlaceResponse> placeResponse = placesClient.findCurrentPlace(request);
                    getCurrentPlace(placeResponse);

                } else {
                    // A local method to request required permissions;0
                    ActivityCompat.requestPermissions(getActivity(), new String[] { Manifest.permission.ACCESS_FINE_LOCATION },ACCESS_FINE_LOCATION_PM);
                    get_location_bt.setEnabled(true);
                }
            }
        });

        add_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(from_destination_edittext.getText().length() > 0 && to_destination_edittext.getText().length() > 0 && from_date != null && to_date != null && from_time != null && to_time != null ){


                    scheduleModel.setTripId(tripId);
                    scheduleModel.setFromDestination(from_destination_edittext.getText().toString().trim());
                    scheduleModel.setToDestination(to_destination_edittext.getText().toString().trim());
                    scheduleModel.setFromDate(from_date);
                    scheduleModel.setToDate(to_date);
                    scheduleModel.setFromTime(from_time);
                    scheduleModel.setToTime(to_time);


                    ref = FirebaseDatabase.getInstance().getReference().child("Schedules");

                    ref.push().setValue(scheduleModel);

                    Navigation.findNavController(v).navigate(R.id.action_addScheduleFragment_self);

                }else if(from_destination_edittext.getText().length() == 0 || to_destination_edittext.getText().length() == 0){
                    Toast.makeText(getActivity(), "Please fill-in the From Destination and To Destination field.", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getActivity(), "Please fill-in the date and time field.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE_F) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
//                Log.i("Place: ", place.getName() + ", " + place.getId());
                from_destination_edittext.setText(place.getName());
                return;
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                return;
            }
            return;
        }else if(requestCode == AUTOCOMPLETE_REQUEST_CODE_T){
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
//                Log.i("Place: ", place.getName() + ", " + place.getId());
                to_destination_edittext.setText(place.getName());
                return;
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                return;
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void onClickDate(String date , TextView textView ){

        MaterialDatePicker.Builder datePickerB =  MaterialDatePicker.Builder.datePicker();
        MaterialDatePicker materialDatePicker = datePickerB.build();

        materialDatePicker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Object selection) {
                textView.setText( date + " " + materialDatePicker.getHeaderText());

                int tv_id = textView.getId();
                switch (tv_id){
                    case R.id.from_date_textView:
                        from_date = materialDatePicker.getHeaderText();
                        return;
                    case R.id.to_date_textView:
                        to_date = materialDatePicker.getHeaderText();
                        return;
                }
            }

        });


        materialDatePicker.show(this.getParentFragmentManager() , "Date Picker");


    }

    private void onClickTime( String date ,TextView textView ){

        int clockFormat = TimeFormat.CLOCK_24H ;

        @NonNull
        MaterialTimePicker.Builder pickerB = new MaterialTimePicker.Builder();
        MaterialTimePicker timePicker = pickerB.setTimeFormat(clockFormat).build();

        timePicker.show(this.getParentFragmentManager() , "Time Picker");


        timePicker.addOnPositiveButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String time = getTime(timePicker.getHour() , timePicker.getMinute());
                textView.setText( date + " " + time );
                int tv_id = textView.getId();
                switch (tv_id){
                    case R.id.from_time_textView:
                        from_time = time;
                        return;
                    case R.id.to_time_textView:
                        to_time = time;
                        return;
                }
            }
        });

    }

    public String getTime( int hour, int minute) {
        String minuteS = minute < 10 ? "0" + minute : "" + minute;
        String hourS = hour < 10 ? "0" + hour : "" + hour;
        String time = hourS + ":" + minuteS ;
        return time;
    }

    public void getCurrentPlace(Task<FindCurrentPlaceResponse> placeResponse){
        placeResponse.addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                FindCurrentPlaceResponse response = task.getResult();
                if(response.getPlaceLikelihoods().size() > 0){
                    from_destination_edittext.setText(response.getPlaceLikelihoods().get(0).getPlace().getName());
                    get_location_bt.setEnabled(true);
                }
            } else {
                Exception exception = task.getException();
                if (exception instanceof ApiException) {
                    ApiException apiException = (ApiException) exception;
                    Log.e(TAG, "Place not found: " + apiException.getStatusCode());
                }
                get_location_bt.setEnabled(true);
            }
        });
    }

}