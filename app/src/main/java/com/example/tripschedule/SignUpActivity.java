package com.example.tripschedule;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import com.example.tripschedule.model.UserModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class SignUpActivity extends AppCompatActivity{

    private TextInputEditText email_editText;
    private TextInputEditText pw_editText;
    private TextInputEditText userName_editText;
    private Button baCK_bt;
    private Button signup_bt;
    private DatabaseReference ref;
    private UserModel userModel;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        email_editText = findViewById(R.id.email_editText);
        pw_editText = findViewById(R.id.pw_editText);
        userName_editText = findViewById(R.id.userName_editText);
        baCK_bt = findViewById(R.id.baCK_bt);
        signup_bt = findViewById(R.id.signup_bt);
        mAuth = FirebaseAuth.getInstance();



        signup_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(userName_editText.getText().length() > 0 && email_editText.getText().length() > 0 && pw_editText.getText().length() > 0 ){

                    if(!Patterns.EMAIL_ADDRESS.matcher(email_editText.getText().toString().trim()).matches()){
                        Toast.makeText(getApplicationContext(), "Please enter the valid email.", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    mAuth.createUserWithEmailAndPassword(email_editText.getText().toString().trim() , pw_editText.getText().toString().trim()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){

                                ref = FirebaseDatabase.getInstance().getReference().child("Users");
                                userModel = new UserModel(FirebaseAuth.getInstance().getCurrentUser().getUid() , userName_editText.getText().toString().trim() , email_editText.getText().toString().trim() , pw_editText.getText().toString().trim() );
                                ref.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(userModel).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            Toast.makeText(getApplicationContext(),"The account is already sign-up.", Toast.LENGTH_SHORT).show();

                                            mAuth.signOut();

                                            Intent i = new Intent(getApplication(), MainActivity.class);
                                            startActivity(i);
                                            finish();

                                        }
                                    }
                                });
                            }else{
                                Toast.makeText(getApplicationContext(),"Sign-up Failed. Please try again.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(),e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });



                }else{
                    Toast.makeText(getApplicationContext(), "You must fill-in the User Name, email and password", Toast.LENGTH_SHORT).show();
                }


            }
        });

        baCK_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplication(), MainActivity.class);
                startActivity(i);
                finish();
            }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(getApplication(), MainActivity.class);
        startActivity(i);
        finish();
    }
}