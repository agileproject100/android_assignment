package com.example.tripschedule.model;

import java.io.Serializable;

public class TripModel implements Serializable {

    private String uid;
    private String tripName;
    private String description;

    public TripModel() {
    }

    public TripModel(String uid, String tripName, String description) {
        this.uid = uid;
        this.tripName = tripName;
        this.description = description;
    }

    public String getUid() {
        return uid;
    }

    public String getTripName() {
        return tripName;
    }

    public String getDescription() {
        return description;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
