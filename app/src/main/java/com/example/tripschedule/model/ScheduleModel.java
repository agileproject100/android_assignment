package com.example.tripschedule.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

public class ScheduleModel  implements Serializable {

    private String tripId;
    private String fromDestination;
    private String toDestination;
    private String fromDate;
    private String toDate;
    private String fromTime;
    private String toTime;

    public ScheduleModel() {
    }

    public ScheduleModel(String tripId, String fromDestination, String toDestination, String fromDate, String toDate, String fromTime, String toTime) {
        this.tripId = tripId;
        this.fromDestination = fromDestination;
        this.toDestination = toDestination;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.fromTime = fromTime;
        this.toTime = toTime;
    }

    public String getTripId() {
        return tripId;
    }

    public String getFromDestination() {
        return fromDestination;
    }

    public String getToDestination() {
        return toDestination;
    }

    public String getFromDate() {
        return fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public String getFromTime() {
        return fromTime;
    }

    public String getToTime() {
        return toTime;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public void setFromDestination(String fromDestination) {
        this.fromDestination = fromDestination;
    }

    public void setToDestination(String toDestination) {
        this.toDestination = toDestination;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }
}
