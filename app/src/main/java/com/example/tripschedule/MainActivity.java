package com.example.tripschedule;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private EditText email_editText;
    private EditText pw_editText;
    private Button signin_bt;
    private Button signup_bt;
    private FirebaseAuth mAuth;

    private Locale myLocale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        email_editText = findViewById(R.id.email_editText);
        pw_editText = findViewById(R.id.pw_editText);
        signin_bt = findViewById(R.id.signin_bt);
        signup_bt = findViewById(R.id.signup_bt);
        mAuth = FirebaseAuth.getInstance();
        changeLanguage();


        signin_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(email_editText.getText().length() == 0 || pw_editText.getText().length() == 0){
                    Toast.makeText(getApplicationContext(), "Please enter the email and password.", Toast.LENGTH_SHORT).show();
                }else {
                    mAuth.signInWithEmailAndPassword(email_editText.getText().toString().trim(), pw_editText.getText().toString().trim()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), "Login Successful.", Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(getApplication(), MenuNavActivity.class);
                                startActivity(i);
                                finish();
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(), "Login Failed. Please try again.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }



            }
        });

        signup_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplication(), SignUpActivity.class);
                startActivity(i);
                finish();
            }
        });

    }


    private void checkUserIsLoggedIn() {
        if(mAuth.getCurrentUser() != null){
            Intent i = new Intent(getApplication(), MenuNavActivity.class);
            startActivity(i);
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.checkUserIsLoggedIn();
    }


    private void changeLanguage(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String lang = prefs.getString("language", "English");

        if(lang.equals("English")){
            myLocale = new Locale("en","US");
        }else{
            myLocale = new Locale("zh","HK");
        }

        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

    }

}